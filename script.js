// Теоретичні питання 
// 1.Це функція, яка є властивістю об'єкта і може бути викликана через об'єкт, до якого вона належить. 
// 2.Значення властивості об'єкта може бути будь-якого типу даних:
// (число, рядок, логічне значення, null, undefined), об'єкти, масиви, функції, символи .
// 3. Об'єкт є посилальним типом даних, що означає, що змінні, 
// які містять об'єкти, не містять сам об'єкт, а лише посилання на нього в пам'яті. 
// Таким чином, якщо ми присвоїмо один об'єктній змінній значення іншої об'єктної змінної, 
// обидві змінні будуть вказувати на один і той самий об'єкт.

// Практичне завдання 1

const product = {
    name: 'Буци', 
    price: 50, 
    discount: 10, 
    
    calculateTotalPrice() {
        const discountAmount = (this.price * this.discount) / 100; 
        const totalPrice = this.price - discountAmount; 
        return totalPrice;
    }
};

const totalPriceWithDiscount = product.calculateTotalPrice();

console.log(`Повна ціна товару "${product.name}" з урахуванням знижки: ${totalPriceWithDiscount}`);

// Практичне завдання 2

function isAlphabetic(input) {
    return /^[A-Za-zА-Яа-яЁёІіЇїЄєҐґ\s]+$/.test(input);
}

function isNumeric(input) {
    return /^\d+$/.test(input);
}

let userName;
do {
    userName = prompt('Введіть своє ім\'я:');
    if (!isAlphabetic(userName)) {
        alert('Введіть ім\'я коректно!');
    }
} while (!isAlphabetic(userName));

let userAge;
do {
    userAge = prompt('Введіть свій вік (тільки цифри):');
    if (!isNumeric(userAge)) {
        alert('Введіть вік тільки цифрами!');
    }
} while (!isNumeric(userAge));

function greeting(person) {
    const message = `Привіт, ${person.name}! Мені ${person.age} років`;
    return message;
}

const message = greeting({ name: userName, age: userAge });
alert(message);

// Практичне завдання 3

function deepClone(obj) {
  if (obj === null || typeof obj !== 'object') {
      return obj;
  }

  const clone = Array.isArray(obj) ? [] : {};

  for (let key in obj) {
      clone[key] = deepClone(obj[key]);
  }

  return clone;
}

const originalObj = {
  name: 'John',
  age: 30,
  address: {
      city: 'New York',
      country: 'USA'
  },
  hobbies: ['reading', 'traveling']
};

const clonedObj = deepClone(originalObj);

console.log('Оригінальний об\'єкт:', originalObj);
console.log('Клонований об\'єкт:', clonedObj);